//
//  socket_io_testappTests.m
//  socket.io-testappTests
//
//  Created by Matthew Clark on 8/8/12.
//  Copyright (c) 2012 Matthew Clark. All rights reserved.
//

#import "socket_io_testappTests.h"

@implementation socket_io_testappTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in socket.io-testappTests");
}

@end
