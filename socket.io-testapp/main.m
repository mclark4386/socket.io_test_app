//
//  main.m
//  socket.io-testapp
//
//  Created by Matthew Clark on 8/8/12.
//  Copyright (c) 2012 Matthew Clark. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SIOTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SIOTAppDelegate class]));
    }
}
