//
//  SIOTViewController.h
//  socket.io-testapp
//
//  Created by Matthew Clark on 8/8/12.
//  Copyright (c) 2012 Matthew Clark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SocketIO.h"

@interface SIOTViewController : UIViewController<SocketIODelegate>
@property (weak, nonatomic) IBOutlet UILabel *group;
@property (weak, nonatomic) IBOutlet UITextField *task;
@property (weak, nonatomic) IBOutlet UITextView *recvTasks;

- (IBAction)sendTask:(id)sender;
@end
