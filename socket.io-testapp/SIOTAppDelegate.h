//
//  SIOTAppDelegate.h
//  socket.io-testapp
//
//  Created by Matthew Clark on 8/8/12.
//  Copyright (c) 2012 Matthew Clark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIOTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
