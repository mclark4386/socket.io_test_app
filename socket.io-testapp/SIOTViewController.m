//
//  SIOTViewController.m
//  socket.io-testapp
//
//  Created by Matthew Clark on 8/8/12.
//  Copyright (c) 2012 Matthew Clark. All rights reserved.
//

#import "SIOTViewController.h"
#import "SIOTConfig.h"

@interface SIOTViewController (){
    SocketIO* mainSocket;
}

@end

@implementation SIOTViewController
@synthesize group;
@synthesize task;
@synthesize recvTasks;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setGroup:nil];
    [self setTask:nil];
    [self setRecvTasks:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    mainSocket = [[SocketIO alloc] initWithDelegate:self];
    [mainSocket connectToHost:kHost onPort:kPort];
    
    group.text = @"blue";
    [mainSocket sendEvent:@"joinGroup" withData:[NSDictionary dictionaryWithObjectsAndKeys:@"blue",@"group", nil] andAcknowledge:^(id argsData) {
        NSLog(@"ack");
        group.text = @"blue";
    }];
}

-(void)socketIO:(SocketIO *)socket didReceiveEvent:(SocketIOPacket *)packet{
    NSLog(@"Event triggered packet: data:%@ type:%@", packet.data, packet.type);
    NSDictionary* response = [NSDictionary dictionaryWithDictionary:packet.dataAsJSON];
    
    if ([response objectForKey:@"name"]) {
        if ([[response objectForKey:@"name"] isEqualToString:@"task"]&&[response objectForKey:@"args"]&&[[response objectForKey:@"args"] count]>0) {
            self.recvTasks.text = [self.recvTasks.text stringByAppendingFormat:@"\n%@",[[response objectForKey:@"args"] objectAtIndex:0]];
        }
    }
}

-(void)socketIO:(SocketIO *)socket didReceiveJSON:(SocketIOPacket *)packet{
    NSLog(@"JSON triggered");
}

-(void)socketIO:(SocketIO *)socket didReceiveMessage:(SocketIOPacket *)packet{
    NSLog(@"Message triggered");
}

-(void)socketIODidConnect:(SocketIO *)socket{
    NSLog(@"Connected");
}

-(void)socketIODidDisconnect:(SocketIO *)socket{
    NSLog(@"Disconnected");
}

- (IBAction)sendTask:(id)sender {
    [task becomeFirstResponder];
    [task resignFirstResponder];
    if ([task.text isEqualToString:@""]) {
        return;
    }
    
    NSLog(@"Sending task(%@) to group:%@",task.text, group.text);
    [mainSocket sendEvent:@"task" withData:[NSDictionary dictionaryWithObjectsAndKeys:group.text,@"group", task.text,@"task", nil]];
}
@end
